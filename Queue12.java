package hw4;
import java.util.*;

public class Queue12<E> implements BoundedQueue<E>
{
  private Deque12<E> deque;
  public Queue12(int n)
  {
    deque=new Deque12<E>(n);
  }
  public int capacity()
  {
    return deque.capacity();//adapter pattern using Deque12
  }
  public int size()
  {
    return deque.size();
  }
  public boolean enqueue(E e)
  {
    return deque.addFront(e);
  }
  public E dequeue()
  {
    return deque.removeBack();//works opposite from Stack12
  }
  public E peek()
  {
    return deque.peekBack();
  }



}
