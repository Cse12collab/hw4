/*
  NAME: Erik Mei
  ID: A12025903
  LOGIN: cs12snp
  GROUP NAMES: Erik Mei, Oscar Bao
*/

package hw4;
import org.junit.*;
import static org.junit.Assert.*;

public class BoundedDequeTesterCopy {
  
  private Deque12<Integer> empty;
  private Deque12<Integer> several;

  @Before
  public void setUp() {
    empty = new Deque12<Integer>(10);
    
  }

  /**
   * Tests the capacity return method
   * Oscar Bao
  **/
  @Test
  public void testCapacity() {
    assertEquals("Testing capacity return...", 10, empty.capacity());
  }

  /**
   * Tests the size return method with an empty, filled, and somewhat
   * filled deque
   * Oscar Bao
  **/
  @Test
  public void testSize() {
    assertEquals("Testing size return...", 0, empty.size());
  }

  /**
   * Tests adding to the front of the deque
   * Has dependency on peekFront method
   * Oscar Bao
  **/
  @Test
  public void testAddFront() {
    Deque12<String> list = new Deque12<String>(10);
    list.addFront("Hello");
    assertEquals("Hello", list.peekFront());
  }

  /**
   * Tests adding to the back of the deque
   * Has dependency on the peekBack method
   * Oscar Bao
  **/
  @Test
  public void testAddBack() {
    Deque12<String> list =  new Deque12<String>(10);
    list.addBack("Goodbye");
    assertEquals("Goodbye", list.peekBack());
  }

  /**
   * Tests front and back peeking
   * Has dependency on add methods
   * Oscar Bao
  **/
  @Test
  public void testPeek() {
    Deque12<String> list = new Deque12<String>(10);
    list.addFront("Front and back");
    assertEquals("Testing front and back with one element",
                  "Front and back", list.peekFront());
    assertEquals("Testing front and back with one element", 
                  "Front and back", list.peekBack());

    list.addFront("Front");
    list.addBack("Back");
    assertEquals("Testing front with 3 elements", "Front", list.peekFront());
    assertEquals("Testing back with 3 elements", "Back", list.peekBack());
  }

  /**
   * Tests null peeking
   * Oscar Bao
  **/
  @Test
  public void testNullPeek() {
    assertEquals("Testing empty peeking", null, empty.peekFront());
  }

  /**
   * Tests front and back removing
   * Has dependency on peeking and add methods
   * Oscar Bao
  **/
  @Test
  public void testRemove() {
    Deque12<Integer> list = new Deque12<Integer>(10);
    list.addFront(1);
    list.addFront(2);
    list.addFront(3);
    list.addFront(4);
    list.addFront(5);
    //list should now be 5 4 3 2 1
    list.removeBack();
    //list should now be 5 4 3 2
    assertEquals("Testing removeBack", new Integer(2), list.peekBack());
    list.removeFront();
    //list should now be 4 3 2 1
    assertEquals("Testing removeFront", new Integer(4), list.peekFront());
  }
  /**
  *Tests dummy initialization
  *Author: Erik Mei
  **/
  @Test
  public void dummyInitialization()
  {
    try
    {
      several = new Deque12<Integer>(-5);
      fail();
    }
    catch(IllegalArgumentException ex)
    {
      //passed
    }
  }
  /**
  *Tests adding to a full Deque
  *Author: Erik Mei
  **/
  @Test
  public void testAddFull()
  {
    for(int i=0;i<empty.capacity();i++)
      empty.addFront(new Integer(i));
    assertEquals("Adding element to full list", false, empty.addFront(new
    Integer(15)));
  }
  
  /**
  *Test adding and peeking to unfilled list
  *Author: Erik Mei
  **/
  @Test
  public void testPeekInUnfilledDeque()
  {
    several=new Deque12<Integer>(5);
    for(int i=0;i<3;i++)
      several.addFront(new Integer(i));
    assertEquals("Checking rear element..", new Integer(0), several.peekBack());
    assertEquals("Checking front element..", new Integer(2), several.peekFront());

  }

}
